package be.onea.example;

import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.DirectChannelSpec;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.integration.jms.JmsOutboundGateway;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@Configuration
@SpringBootApplication
public class ExampleApplication {
	
	private static final String BROKER_URL = "tcp://localhost:61616";
	
	public static void main(String[] args) {
		SpringApplication.run(ExampleApplication.class, args);
	}
	
	private ConnectionFactory connectionFactory = createConnectionFactory();
	private MessageConverter messageConverter = createMessageConverter();
	
	private ConnectionFactory createConnectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(BROKER_URL);
		return connectionFactory;
	}
	
	private MessageConverter createMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
	
	public JmsOutboundGateway createOutboundGateway() {
		return Jms.outboundGateway(connectionFactory)
				.requestDestination("ExampleQueue")	// Put messages on the queue 'ExampleQueue'
				.jmsMessageConverter(messageConverter)
				.correlationKey("JMSCorrelationID")
				.get();
	}
	
	@Bean
	public IntegrationFlow flow() {
		return IntegrationFlows
			.from(new IntService(), "getValue", c -> c.poller(Pollers.fixedDelay(1000))) // Generates new integer every second
			.handle(createOutboundGateway()) // I expect this gateway to receive a reply after the integer has been enqueued and the flow to continue to the next line
			.handle(x -> System.out.println("Succesfully queued '" + x + "'")) // This never gets printed: instead I get 'failed to receive JMS response within timeout of: 5000ms'
			.get();
	}
	
	class IntService {
		private AtomicInteger atomicInt = new AtomicInteger();
		public Integer getValue() {
			return atomicInt.getAndIncrement();
		}
	}
	
}
