# Example Application

This is an example application that illustrates a problem I'm having with *spring integration*.

## Goal

The goal I'm trying to achieve is to send a message to a JMS message broker (in my case ActiveMQ) and to perform some action **only** when the message was successfully delivered to the broker.
In case something goes wrong when delivering the message (i.e. connection problems, the sender throws an exception, etc.) nothing needs to happen.

## How I'm trying to achieve this

I set up an *outbound JMS gateway* that connects to the _ActiveMQ_ instance.
From my understanding an outbound gateway sets up two-way communication with an external system (in this case ActiveMQ).

What I **would expect** is that this gateway receives a reply after the message is delivered, after which it continues executing the rest of the flow.

However, **what really happens** is that the message gets delivered to the broker, the gateway starts listening for a reply and that listener throws a timeout exception after 5 seconds.

## In this example

In this example I create a very simple flow:

```java
@Bean
public IntegrationFlow flow() {
	return IntegrationFlows
		.from(new IntService(), "getValue", c -> c.poller(Pollers.fixedDelay(1000)))
		.handle(createOutboundGateway())
		.handle(x -> System.out.println("Succesfully queued '" + x + "'"))
		.get();
}
```

Line by line:

```
.from(new IntService(), "getValue", c -> c.poller(Pollers.fixedDelay(1000)))
```

Creates a service that returns a new integer every 1 second.
This integer gets wrapped inside a message which then goes through the rest of the flow.

```
.handle(createOutboundGateway())
```
Here this message containing the int gets handled by a JMS gateway.
I expect this gateway to do the following:

1. Send a request message to the message broker containing the wrapped int message
2. The message broker receives this message and stores it in a queue (this does happen)
3. The message broker then sends a reply to the gateway
4. The gateway is listening for a reply and upon receiving the reply continues with the flow

However, steps 3 and 4 don't seem to get executed, instead the gateway starts listening and after 5 seconds I get a timeout

```
.handle(x -> System.out.println("Succesfully queued '" + x + "'"))
```

I would expect this handler to receive the reply from the gateway and print it to stdout, unfortunately this never gets executed due to the timeout exception that gets thrown.

## Executing this example

To see this example running first start an instance of ActiveMQ on port 61616, e.g. if you have docker installed you can simply run:

```
$ docker run --name='activemq' -it --rm -p1883:1883 -p5672:5672 \
-p8161:8161 -p61613:61613 -p61614:61614 -p61616:61616 \
webcenter/activemq:latest
```

Next simply run gradleBoot using the provided gradle wrapper:

``
$ ./gradlew bootRun
``